package main

type UserRole uint8

const (
	Admin UserRole = 1
	Usual UserRole = 2
	Prev  UserRole = 3
	Super UserRole = 4
)

type User struct {
	Name  string
	Roles map[UserRole]struct{}
}

func (u *User) AddRole(r UserRole) {
	//   1 byte   0 byte, max 4 bytes
	u.Roles[r] = struct{}{}
}
func (u *User) HasRole(r UserRole) bool {
	_, ok := u.Roles[r]
	return ok
}

type UserRoleB byte

const (
	AdminB UserRoleB = 1 << iota
	UsualB
	PrevB
	SuperB
)

type UserB struct {
	Name  string
	Roles UserRoleB
}

func (u *UserB) AddRole(r UserRoleB) {
	// 1 byte
	u.Roles = u.Roles | r
}
func (u *UserB) HasRole(r UserRoleB) bool {
	return u.Roles&r != 0
}
func (u *UserB) DeleteRole(r UserRoleB) {

	res := ^r & u.Roles
	u.Roles = res
}
func (u *UserB) GetMyRoles() {

	println("Имеющиеся роли:")
	for i := 0; i < 4; i++ {
		role := u.Roles >> i
		if role&1 == 1 {
			println(role & 1 << i)
		}
	}
}
func (u *UserB) GetNotMyRoles() {
	println("Отсутствующие роли:")
	for i := 0; i < 4; i++ {
		role := ^u.Roles >> i
		if role&1 == 1 {
			println(role & 1 << i)
		}
	}
}

func main() {
	user := UserB{"Ilia", UserRoleB(0)}

	user.AddRole(2)
	user.AddRole(4)
	user.GetMyRoles()
	user.GetNotMyRoles()

	user.DeleteRole(4)
	user.GetMyRoles()
}
