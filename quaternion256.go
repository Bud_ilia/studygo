package main

import (
	"fmt"
	"math"
)

type Quaternion256 struct {
	S float64
	X float64
	Y float64
	Z float64
}

func convertNumberToStringWithSign(value float64) string {
	if value < 0 {
		return fmt.Sprintf("%.4g", value)
	}
	return fmt.Sprintf("+%.4g", value)
}

func (num Quaternion256) String() string {
	return "(" + fmt.Sprintf("%.4g", num.S) +
		convertNumberToStringWithSign(num.X) + "i" +
		convertNumberToStringWithSign(num.Y) + "j" +
		convertNumberToStringWithSign(num.Z) + "k)"
}

func (num Quaternion256) Add(arg Quaternion256) Quaternion256 {
	return Quaternion256{
		num.S + arg.S,
		num.X + arg.X,
		num.Y + arg.Y,
		num.Z + arg.Z,
	}
}

func (num Quaternion256) Subtract(arg Quaternion256) Quaternion256 {
	return Quaternion256{
		num.S - arg.S,
		num.X - arg.X,
		num.Y - arg.Y,
		num.Z - arg.Z,
	}
}

func (num Quaternion256) MultipleAndReturnTwoNumbers(arg Quaternion256) (Quaternion256, Quaternion256) {

	// вещественный кватернион
	realNum := Quaternion256{
		num.S*arg.S - num.X*arg.X - num.Y*arg.Y - num.Z*arg.Z,
		0,
		0,
		0,
	}

	// чистый кватернион
	clean := Quaternion256{
		0,
		num.S*arg.X + arg.S*num.X + num.Y*arg.Z - arg.Y*num.Z,
		num.S*arg.Y + arg.S*num.Y + num.Z*arg.X - arg.Z*num.X,
		num.S*arg.Z + arg.S*num.Z + num.X*arg.Y - arg.X*num.Y,
	}
	return realNum, clean
}

func (num Quaternion256) MultipleAndReturnOneNumber(arg Quaternion256) Quaternion256 {
	return Quaternion256{
		num.S*arg.S - num.X*arg.X - num.Y*arg.Y - num.Z*arg.Z,
		num.S*arg.X + arg.S*num.X + num.Y*arg.Z - arg.Y*num.Z,
		num.S*arg.Y + arg.S*num.Y + num.Z*arg.X - arg.Z*num.X,
		num.S*arg.Z + arg.S*num.Z + num.X*arg.Y - arg.X*num.Y,
	}
}

func (num Quaternion256) CalcNorm() float64 {
	return math.Sqrt(num.S*num.S + num.X*num.X + num.Y*num.Y + num.Z*num.Z)
}

func (num Quaternion256) Normalize() Quaternion256 {
	norm := num.CalcNorm()
	return Quaternion256{
		num.S / norm,
		num.X / norm,
		num.Y / norm,
		num.Z / norm,
	}
}

func (num Quaternion256) ScalarMultiplication(arg Quaternion256) float64 {
	return num.S*arg.S + num.X*arg.X + num.Y*arg.Y + num.Z*arg.Z
}

func (num Quaternion256) CalcCosBetweenNums(arg Quaternion256) float64 {
	return num.ScalarMultiplication(arg) / (num.CalcNorm() * arg.CalcNorm())
}
