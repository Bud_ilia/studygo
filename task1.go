package main

import (
	"fmt"
	"os"
)

// https://acmp.ru/index.asp?main=task&id_task=3

func calc(value int64) int64 {
	if value%10 == 5 {
		if value == 5 {
			return 25
		} else {
			res := value / 10
			res = res * (res + 1)
			res = res*10 + 2
			res = res*10 + 5
			return res
		}
	}
	return value * value
}

func main() {
	var value int64
	fmt.Println("Введите число:")
	fmt.Fscan(os.Stdin, &value)
	fmt.Println(calc(value))
}
