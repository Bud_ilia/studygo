package main

import (
	"fmt"
	"os"
)

// https://acmp.ru/index.asp?main=task&id_task=7

// поискать что-нибуть для длинной арифметики или реализовать ее самому.
func calcMaxHeap(heap1, heap2, heap3 int64) int64 {
	if heap1 >= heap2 && heap1 >= heap3 {
		return heap1
	}
	if heap2 >= heap1 && heap2 >= heap3 {
		return heap2
	}
	return heap3
}

func main() {
	var heap1, heap2, heap3 int64
	fmt.Fscan(os.Stdin, &heap1, &heap2, &heap3)
	fmt.Println(calcMaxHeap(heap1, heap2, heap3))
}
