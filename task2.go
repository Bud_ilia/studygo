package main

import (
	"fmt"
	"os"
)

// https://acmp.ru/index.asp?main=task&id_task=4

func calcNum(value int64) int64 {
	return (value*10+9)*10 + (9 - value)
}

func main() {
	var value int64
	fmt.Println("Введите число")
	fmt.Fscan(os.Stdin, &value)
	fmt.Println(calcNum(value))
}
