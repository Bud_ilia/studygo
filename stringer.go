package main

import (
	"fmt"
	"strconv"
	"strings"
)

type IPAddr [4]byte

func (ip IPAddr) String() string {

	values := []string{
		strconv.Itoa(int(ip[0])),
		strconv.Itoa(int(ip[1])),
		strconv.Itoa(int(ip[2])),
		strconv.Itoa(int(ip[3])),
	}

	return strings.Join(values, ".")
}

func main() {
	hosts := map[string]IPAddr{
		"loopback":  {127, 0, 0, 1},
		"googleDNS": {8, 8, 8, 8},
	}
	for name, ip := range hosts {
		fmt.Printf("%v: %v\n", name, ip)
	}
}
