package main

import (
	"fmt"
	"reflect"
)

type Student struct {
	Name string `desc:"Name of Student"`
	Age  int    `desc:"Age of Student"`
}

func (s Student) getTagsAndValues() {
	elem := reflect.ValueOf(&s).Elem()
	typeStr := elem.Type()

	for i := 0; i < elem.NumField(); i++ {
		f := elem.Field(i)
		value := f.Convert(f.Type())
		fmt.Printf("%s = %v\n", typeStr.Field(i).Tag.Get("desc"), value)
	}

}

func (s Student) getTagsAndValuesForInt() {
	elem := reflect.ValueOf(&s).Elem()
	typeStr := elem.Type()

	for i := 0; i < elem.NumField(); i++ {
		f := elem.Field(i)
		if f.Kind() == reflect.Int {
			value := f.Convert(f.Type())
			fmt.Printf("%s = %v\n", typeStr.Field(i).Tag.Get("desc"), value)
		}
	}
}

func (s *Student) updateFields(fieldName string, fieldValue string) {
	elem := reflect.ValueOf(s).Elem()
	typeStr := elem.Type()

	for i := 0; i < elem.NumField(); i++ {
		f := elem.Field(i)

		if typeStr.Field(i).Name == fieldName {
			updVal := reflect.ValueOf(fieldValue).Convert(f.Type())
			f.Set(updVal)
		}
	}
}

func main() {
	st := &Student{Name: "Вася", Age: 5}

	st.getTagsAndValues()
	println()
	st.getTagsAndValuesForInt()
	println()
	st.updateFields("Name", "Ilia")
	println(st.Name)
}
