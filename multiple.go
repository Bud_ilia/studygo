package main

import "fmt"

func multiple(s []int, c chan int64) {
	var res int64 = 1
	for _, value := range s {
		res = res * int64(value)
	}
	c <- res
}

func main() {
	var c = make(chan int64)
	s := []int{7, 2, 8, -9, 4, 5}
	go multiple(s[:2], c)
	go multiple(s[1:3], c)
	a, b := <-c, <-c
	fmt.Println(a, " ", b)
}
