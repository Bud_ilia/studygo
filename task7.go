package main

import (
	"fmt"
	"os"
)

//https://acmp.ru/index.asp?main=task&id_task=38

func getMaxNum(array []int64, moveNumber int) (int, int64) {
	index := len(array) - 1
	switch {
	case array[0] > array[index]:
		return 0, array[0]
	case array[0] < array[index]:
		return index, array[index]
	case array[0] == array[index] && moveNumber&2 == 0:
		return 0, array[0]
	default:
		return index, array[index]

	}
}

func deleteElement(array []int64, index int) []int64 {
	res := make([]int64, 0)
	for i, value := range array {
		if i == index {
			continue
		}
		res = append(res, value)
	}
	return res
}

func game(array []int64) (int64, int64) {
	lenArr := len(array)
	var sum1, sum2 int64
	for i := 0; i < lenArr; i++ {
		maxIndex, maxValue := getMaxNum(array, i)
		array = deleteElement(array, maxIndex)
		if i%2 == 0 {
			sum1 = sum1 + maxValue
		} else {
			sum2 = sum2 + maxValue
		}
	}
	return sum1, sum2
}

func getWinner(array []int64) int8 {
	sum1, sum2 := game(array)
	switch {
	case sum1 > sum2:
		return 1
	case sum2 > sum1:
		return 2
	default:
		return 0
	}
}

func main() {
	var count int64
	fmt.Println("Введите длину массива:")
	fmt.Fscan(os.Stdin, &count)
	fmt.Println("Введите числа:")
	var array = make([]int64, count)
	for index, _ := range array {
		fmt.Fscan(os.Stdin, &array[index])
	}
	println(getWinner(array))
}
