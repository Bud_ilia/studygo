package main

import (
	"context"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"io/ioutil"
	"os"
	"strings"
	"sync"
)

type worker struct {
	inputFilePath  string
	outputFilePath string
	command        string
}

func (w *worker) OpenImage() image.Image {
	f, err := os.Open(w.inputFilePath)
	if err != nil {
		return nil
	}

	defer f.Close()

	img, _, err := image.Decode(f)
	if err != nil {
		return nil
	}

	return img
}

func (w *worker) SaveImage(img image.Image) {
	f, err := os.Create(w.outputFilePath)
	if err != nil {
		return
	}
	defer f.Close()
	err = png.Encode(f, img)
}

func (w *worker) reduceColor(img image.Image) image.Image {
	top := img.Bounds().Min.Y
	left := img.Bounds().Min.X
	bottom := img.Bounds().Max.Y
	right := img.Bounds().Max.X

	resImage := image.NewRGBA(img.Bounds())

	for y := top; y < bottom; y++ {
		for x := left; x < right; x++ {
			cl := img.At(x, y)
			r, g, b, _ := cl.RGBA()
			m := uint8(b+g+r) / 3
			rgba := color.RGBA{R: m, G: m, B: m, A: 255}

			resImage.Set(x, y, rgba)
		}
	}

	return resImage
}

func (w worker) DoCommand(img image.Image) image.Image {
	if w.command == "none" {
		return img
	} else if w.command == "reduce-color" {
		return w.reduceColor(img)
	}
	return img
}

var wg sync.WaitGroup

func (w *worker) DoWork(ctx context.Context, ch chan int, i int) {

	img := w.OpenImage()
	img = w.DoCommand(img)
	ch <- i
	w.SaveImage(img)

	<-ctx.Done()
}

func main() {

	ctx, cancel := context.WithCancel(context.Background())

	data, err := ioutil.ReadFile("hello.txt")
	if err != nil {
		fmt.Println(err)
	}

	splitData := strings.Split(string(data), "\n")

	ch := make(chan int, 1)

	for i := 0; i < len(splitData); i++ {
		commands := strings.Split(splitData[i], " ")

		w := worker{
			inputFilePath:  commands[0],
			command:        commands[1],
			outputFilePath: commands[2],
		}

		go w.DoWork(ctx, ch, i)
		println(commands[0], " ", commands[1], " ", commands[2])
	}

	_, ok := <-ch

	if ok {
		cancel()
	}
}
