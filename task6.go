package main

import (
	"fmt"
	"os"
)

//https://acmp.ru/index.asp?main=task&id_task=23

func sumDivs(number int) int {
	var sum int = 0
	for i := 1; i <= number; i++ {
		if number%i == 0 {
			sum = sum + i
		}
	}
	return sum
}

func main() {
	var number int
	fmt.Println("Введите число:")
	fmt.Fscan(os.Stdin, &number)
	fmt.Println(sumDivs(number))

}
