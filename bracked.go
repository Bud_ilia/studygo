package main

import (
	"fmt"
	"os"
)

func isValid(arg string) bool {

	var count = 0
	for _, value := range arg {
		if value == 41 {
			count++
		}

		if value == 40 {
			count--
		}
	}
	if count == 0 {
		return true
	}
	return false
}

func main() {
	var str string
	fmt.Println("Введите строку")
	fmt.Fscan(os.Stdin, &str)
	fmt.Println(isValid(str))
}
