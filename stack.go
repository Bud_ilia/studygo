package main

import (
	"fmt"
	"os"
)

type stacker interface{}

type stack struct {
	value stacker
	next  *stack
}

func (s *stack) getLast() *stack {

	if s == nil {
		return nil
	}

	if s.next == nil {
		return s
	}

	tmp := s

	for tmp.next != nil {
		tmp = tmp.next
	}

	return tmp
}

func (s *stack) push(value stacker) {
	tmp := s.getLast()
	if tmp == nil {
		tmp = &stack{value: value, next: nil}
		return
	}
	tmp.next = &stack{value: value, next: nil}
}

func (s *stack) pop() stacker {

	tmp := s
	if tmp == nil {
		return nil
	}

	if tmp.next == nil {
		value := tmp.value
		tmp = nil
		return value
	}

	for tmp.next.next != nil {
		tmp = tmp.next
	}

	var value = tmp.next.value
	tmp.next = nil
	return value
}

func (s *stack) printAll() {
	tmp := s
	if s == nil {
		return
	}
	fmt.Print(tmp.value, " ")
	for tmp.next != nil {
		tmp = tmp.next
		fmt.Print(tmp.value, " ")
	}
	fmt.Println()
}

func parseString(value string) *stack {
	s := &stack{}
	for i := 0; i < len(value); i++ {
		s.push(value[i])
	}
	return s
}

func isValid(value string) bool {
	s := parseString(value)

	count := int64(0)

	for i := 0; i < len(value); i++ {
		tmp := s.pop()
		if tmp == uint8(40) {
			count++
		}
		if tmp == uint8(41) {
			count--
		}
	}
	return count == 0
}

func main() {
	var str string
	fmt.Println("Введите строку")
	fmt.Fscan(os.Stdin, &str)
	fmt.Println(isValid(str))
}
