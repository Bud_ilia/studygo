package main

import (
	"io"
	"os"
	"strings"
)

type rot13Reader struct {
	r io.Reader
}

func (r rot13Reader) Read(p []byte) (n int, err error) {
	n, err = r.r.Read(p)
	for i := 0; i < len(p); i++ {
		p[i] = convertToRot13(p[i])
	}
	return
}

func convertToRot13(element byte) byte {
	switch {
	case (element >= 'A' && element < 'N') || (element >= 'a' && element < 'n'):
		element = element + 13
	case (element >= 'N' && element <= 'Z') || (element >= 'n' && element <= 'z'):
		element = element - 13
	}
	return element
}

func main() {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	_, _ = io.Copy(os.Stdout, &r)
}
