package main

import (
	"fmt"
	"math"
	"os"
)

// https://acmp.ru/index.asp?main=task&id_task=9

func sumPositive(array []int64) int64 {
	var sum int64 = 0
	for _, value := range array {
		if value > 0 {
			sum = sum + value
		}
	}
	return sum
}

func findMax(array []int64) (int, int64) {
	var tmpValue int64 = -1
	var tmpIndex int = -1
	for index, value := range array {
		if value >= tmpValue {
			tmpValue = value
			tmpIndex = index
		}
	}
	return tmpIndex, tmpValue
}

func findMin(array []int64) (int, int64) {
	var tmpValue int64 = -1
	var tmpIndex int = -1
	for index, value := range array {
		if value <= tmpValue {
			tmpValue = value
			tmpIndex = index
		}
	}
	return tmpIndex, tmpValue
}

func preparePairIndexes(array []int64) (int, int) {
	minIndex, _ := findMin(array)
	maxIndex, _ := findMax(array)
	return int(math.Min(float64(minIndex), float64(maxIndex))), int(math.Max(float64(minIndex), float64(maxIndex)))
}

func multipleWithoutMinAndMax(array []int64) int64 {

	var tmp int64 = 1
	min, max := preparePairIndexes(array)
	for index, value := range array {
		if (index > min) && (index < max) {
			tmp = tmp * value
		}

	}
	return tmp
}

func main() {
	var count int64
	fmt.Println("Введите длину массива:")
	fmt.Fscan(os.Stdin, &count)
	fmt.Println("Введите числа:")
	var array = make([]int64, count)
	for index, _ := range array {
		fmt.Fscan(os.Stdin, &array[index])
	}
	fmt.Println(sumPositive(array), " ", multipleWithoutMinAndMax(array))
}
