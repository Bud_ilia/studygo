package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"sync"
)

var words = make(map[uint64]string)
var mutex = new(sync.Mutex)

// --security-opt apparmor=unconfined --cap-add=SYS_PTRACE

type Word struct {
	Index uint64
	Value string
}

func getWords(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(words)
}

func getWord(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	for index, _ := range words {
		word := Word{Index: index, Value: words[index]}
		params := mux.Vars(r)
		if fmt.Sprintf("%d", word.Index) == params["index"] {
			_ = json.NewEncoder(w).Encode(word)
		}
	}
}

func createOrUpdateWord(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var word Word
	json.NewDecoder(r.Body).Decode(&word)
	mutex.Lock()
	words[word.Index] = word.Value
	mutex.Unlock()
	json.NewEncoder(w).Encode(words)
}

func main() {

	r := mux.NewRouter()

	words[0] = "word1"
	words[1] = "book"
	words[2] = "dolor"
	r.HandleFunc("/words/", getWords).Methods("GET")
	r.HandleFunc("/words/{index}", getWord).Methods("GET")
	r.HandleFunc("/words/", createOrUpdateWord).Methods("POST")
	log.Fatal(http.ListenAndServe(":8000", r))

}
