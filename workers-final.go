package main

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"io/ioutil"
	"os"
	"strings"
)

type worker struct {
	inputFilePath  string
	outputFilePath string
	command        string
}

func (worker *worker) OpenImage() image.Image {
	f, error := os.Open(worker.inputFilePath)
	if error != nil {
		return nil
	}

	defer f.Close()

	img, _, err := image.Decode(f)
	if err != nil {
		return nil
	}

	return img
}

func (worker *worker) SaveImage(img *image.Image) {
	f, err := os.Create(worker.outputFilePath)
	defer f.Close()
	if err != nil {
		return
	}
	err = png.Encode(f, *img)
}

func (worker *worker) reduceColor(img image.Image) image.Image {
	top := img.Bounds().Min.Y
	left := img.Bounds().Min.X
	bottom := img.Bounds().Max.Y
	right := img.Bounds().Max.X

	resImage := image.NewRGBA(img.Bounds())

	for y := top; y < bottom; y++ {
		for x := left; x < right; x++ {
			cl := img.At(x, y)
			r, g, b, a := cl.RGBA()

			rgba := color.RGBA{R: uint8(0.99 * float32(r)), G: uint8(0.587 * float32(g)), B: uint8(0.114 * float32(b)), A: uint8(a)}

			resImage.Set(x, y, rgba)
		}
	}

	return resImage
}

func (worker worker) DoCommand(img image.Image) image.Image {
	if worker.command == "none" {
		return img
	} else if worker.command == "reduce-color" {
		return worker.reduceColor(img)
	}
	return img
}

func (worker *worker) DoWork() {
	img := worker.OpenImage()
	img = worker.DoCommand(img)
	worker.SaveImage(&img)
}

//
//func DoSome(ctx context.Context){
//	ctx.Value()
//}

func main() {
	//var worker = worker{
	//	outputFilePath: "/home/ilya/Изображения/tiger_001.png",
	//	inputFilePath:  "/home/ilya/Изображения/Другое/tiger_001.png",
	//	command:        "",
	//}

	data, err := ioutil.ReadFile("hello.txt")
	if err != nil {
		fmt.Println(err)
	}

	splitData := strings.Split(string(data), "\n")

	for i := 0; i < len(splitData); i++ {
		commands := strings.Split(splitData[i], " ")

		w := worker{
			inputFilePath:  commands[0],
			command:        commands[1],
			outputFilePath: commands[2],
		}
		w.DoWork()
		println(commands[0], " ", commands[1], " ", commands[2])
	}

	//worker.DoWork()

}

// https://sodocumentation.net/go/topic/10557/images
