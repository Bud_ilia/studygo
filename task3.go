package main

import (
	"fmt"
	"os"
)

func separateDates(list []int) ([]int, []int) {
	even := make([]int, 0)
	odd := make([]int, 0)

	for _, value := range list {
		if value%2 == 1 {
			odd = append(odd, value)
		} else {
			even = append(even, value)
		}
	}
	return even, odd
}

func main() {
	fmt.Println("Введите количество элементов целочисленного массива")
	var count int8
	fmt.Fscan(os.Stdin, &count)
	fmt.Println("Вводите элементы массива")
	var arr = make([]int, count)
	for index, _ := range arr {
		fmt.Fscan(os.Stdin, &arr[index])
	}

	even, odd := separateDates(arr)

	fmt.Printf("%v\n", odd)
	fmt.Printf("%v\n", even)
	if len(even) >= len(odd) {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
