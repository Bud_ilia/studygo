package main

import (
	"strings"
)

func WordCount(s string) map[string]int {
	res := make(map[string]int)

	for _, value := range strings.Split(s, " ") {
		res[value] = res[value] + 1
	}
	return res
}

func main() {
	for key, value := range WordCount("I am learning Go!") {
		println(key, value)
	}
}
